import org.apache.commons.lang3.SystemUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Sample {
    private static String URL = "https://www.youtube.com/";

    public static void main(String[] args) {

        Boolean isLinux = SystemUtils.IS_OS_LINUX;
        Boolean isWindows = SystemUtils.IS_OS_WINDOWS;
        // Path to web driver
        System.setProperty("webdriver.chrome.driver","src/main/resources/chromedriver");

        WebDriver driver = new ChromeDriver();
        driver.navigate().to(URL);

        WebElement in_search = driver.findElement(By.xpath("//*[@id=\"search\"]"));
        in_search.sendKeys("Hello Its me");

        WebElement btn_search = driver.findElement(By.xpath("//*[@id=\"search-icon-legacy\"]"));
        btn_search.click();

        WebElement play_video = driver.findElement(By.xpath("/html/body/ytd-app/div/ytd-page-manager/ytd-search/div[1]/ytd-two-column-search-results-renderer/div/ytd-section-list-renderer/div[2]/ytd-item-section-renderer/div[3]/ytd-video-renderer[1]/div[1]/ytd-thumbnail/a/yt-img-shadow/img"));
        play_video.click();
    }

}
