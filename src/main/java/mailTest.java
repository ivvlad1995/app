import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

// kind.p37i4 - login
// A123456d
public class mailTest {
    private static String URL = "https://www.yandex.ru";

    public static void main(String[] args) throws InterruptedException {
        int i = 0;
        while(i < 25){
            WebDriver driver = new ChromeDriver();
            WebDriverWait wait = new WebDriverWait(driver, 4);
            driver.get(URL);

            WebElement loginBtn = wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("Войти в почту")));
            loginBtn.click();

            WebElement loginInput = wait.until(ExpectedConditions.visibilityOfElementLocated(By.
                    cssSelector("form div input")));
            loginInput.sendKeys("kind.p37i4");
            loginInput.submit();

            WebElement passInput = wait.until(ExpectedConditions.visibilityOfElementLocated(By.
                    cssSelector("form div div div input")));

            passInput.sendKeys("A123456d");
            passInput.submit();

            WebElement newLetter = wait.until(ExpectedConditions.visibilityOfElementLocated(By.
                    cssSelector("a[href='#compose']")));

            newLetter.click();

            WebElement hwo = wait.until(ExpectedConditions.visibilityOfElementLocated(By.
                    cssSelector("div[name='to']")));
            hwo.click();
            hwo.sendKeys("iv.vlad.1995@yandex.ru");

            WebElement textBox = wait.until(ExpectedConditions.visibilityOfElementLocated(By.
                    cssSelector("div[role='textbox']")));

            textBox.sendKeys("Привет, мир!");

            String selectAll = Keys.chord(Keys.CONTROL, Keys.ENTER);
            driver.findElement(By.tagName("html")).sendKeys(selectAll);
            Thread.sleep(1500);
            driver.quit();
            i++;
        }
    }
}
