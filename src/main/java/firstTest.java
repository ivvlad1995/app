import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.List;

public class firstTest {
    // https://kreisfahrer.gitbooks.io/selenium-webdriver/content/webdriver_api_slozhnie_vzaimodeistviya/lokatori_css,_xpath,_jquery.html
    private static String URL = "https://www.youtube.com";
    public static void main(String[] args) {
        WebDriver driver = new ChromeDriver();
        WebDriverWait wait = new WebDriverWait(driver, 3);
        driver.get(URL);
        WebElement search = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("search")));
        search.sendKeys("Marmok");
        WebElement submit = driver.findElement(By.id("search-icon-legacy"));
        submit.click();
        WebElement subscribe = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("ytd-channel-renderer a")));
        subscribe.click();
        WebElement containerPaperTabs = wait.until(ExpectedConditions.visibilityOfElementLocated(By
                .cssSelector("app-toolbar div")
        ));
        List<WebElement> paperTabs = containerPaperTabs.findElements(By.cssSelector("paper-tab"));
        paperTabs.get(5).click();

        WebElement containerDescription = wait.until(ExpectedConditions.visibilityOfElementLocated(By
                .cssSelector("ytd-channel-about-metadata-renderer div div")
        ));
        List<WebElement> listYtFormattedString = containerDescription.findElements(By.cssSelector("yt-formatted-string"));
        System.out.println(listYtFormattedString.get(1).getText());
        driver.quit();
    }
}